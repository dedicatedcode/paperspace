package com.dedicatedcode.paperspace;

import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public final class AssertionUtils {
    private AssertionUtils() {
    }

    public enum DateTimeComparePrecision {
        SECONDS
    }

    public static void assertDateTimeEquals(LocalDateTime expected, LocalDateTime actual, DateTimeComparePrecision precision) {
        if (Objects.requireNonNull(precision) == DateTimeComparePrecision.SECONDS) {
            assertEquals(expected.withNano(0), actual.withNano(0));
        } else {
            fail("Unhandled precision");
        }
    }
}
