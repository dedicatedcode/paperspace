package com.dedicatedcode.paperspace;

import com.dedicatedcode.paperspace.model.Binary;
import com.dedicatedcode.paperspace.model.Document;
import com.dedicatedcode.paperspace.model.OCRState;
import com.dedicatedcode.paperspace.search.PaginationService;
import com.dedicatedcode.paperspace.web.DocumentResponse;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.font.Standard14Fonts;
import org.springframework.util.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class TestHelper {
    private static final PaginationService paginationService = new PaginationService();

    private TestHelper() {
    }

    public static String rand(String prefix) {
        return prefix + UUID.randomUUID();
    }

    public static String rand() {
        return rand("");
    }

    public static TestFile randPdf() {
        return randPdf(1);
    }

    public static TestFile randPdf(int pages) {
        PDFont font = new PDType1Font(Standard14Fonts.FontName.HELVETICA_BOLD);

        try (PDDocument doc = new PDDocument()) {
            File target = Files.createTempFile("test_", ".pdf").toFile();
            for (int i = 0; i < pages; i++) {
                PDPage page = new PDPage();
                doc.addPage(page);
                try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                    contents.beginText();
                    contents.setFont(font, 12);
                    contents.newLineAtOffset(100, 700);
                    contents.showText("PAGE " + i + " -TEST: " + UUID.randomUUID());
                    contents.endText();
                }
            }

            doc.save(target);
            InputStream is = new FileInputStream(target);
            return new TestFile(target, DigestUtils.md5DigestAsHex(is));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Binary randBinary(int pages) {
        TestFile testFile = randPdf(pages);
        return new Binary(UUID.randomUUID(), LocalDateTime.now(), testFile.file.getAbsolutePath(), testFile.getHash(), "application/pdf", testFile.file.length(), OCRState.PROCESSED);
    }

    public static Binary randBinary() {
        return randBinary(1);
    }


    static SearchResponse createSearchResponse(String queryString, int numberOfDocument) {
        List<DocumentResponse> items = IntStream.range(1, numberOfDocument + 1)
                .mapToObj(value -> {
                            Document document = new Document(UUID.randomUUID(), LocalDateTime.now().plusSeconds(value), "Test Document " + value, "Test Description " + value, randBinary(), "Test Content", Collections.emptyList(), Collections.emptyList());
                            return new DocumentResponse(document);
                        }
                ).collect(Collectors.toList());
        Map<String, Object> pagination = paginationService.createPagination(queryString, 0, numberOfDocument, numberOfDocument, items, Collections.emptyList(), Collections.emptyList());
        List<SearchResponse.TagFacet> tags = null;
        List<SearchResponse.FilterFacet> filters = null;
        return new SearchResponse(items, pagination, tags, filters);
    }

    public static class TestFile {
        private final File file;
        private final String hash;

        public TestFile(File file, String hash) {
            this.file = file;
            this.hash = hash;
        }

        public File getFile() {
            return file;
        }

        public String getHash() {
            return hash;
        }
    }
}
