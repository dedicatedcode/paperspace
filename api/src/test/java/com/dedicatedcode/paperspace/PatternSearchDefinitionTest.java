package com.dedicatedcode.paperspace;

import com.dedicatedcode.paperspace.tags.PatternSearchDefinition;
import com.dedicatedcode.paperspace.tags.Tag;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class PatternSearchDefinitionTest {


    @Test
    void shouldGenerateSimpleClause() {
        PatternSearchDefinition definition = new Tag(UUID.randomUUID(), "Test", "TEST").getPatternSearchDefinition();
        assertEquals(1, definition.getArgs().length);
        assertEquals("*TEST*", definition.getArgs()[0]);
        assertEquals("(LOWER(content) GLOB LOWER(?))", definition.getWhereClause());
    }

    @Test
    void shouldGenerateSimpleAndClause() {
        PatternSearchDefinition definition = new Tag(UUID.randomUUID(), "Test", "TEST, TEST_OTHER").getPatternSearchDefinition();
        assertEquals(2, definition.getArgs().length);
        assertEquals("*TEST*", definition.getArgs()[0]);
        assertEquals("*TEST_OTHER*", definition.getArgs()[1]);
        assertEquals("(LOWER(content) GLOB LOWER(?) AND LOWER(content) GLOB LOWER(?))", definition.getWhereClause());
    }

    @Test
    void shouldGenerateCombinedNotClause() {
        PatternSearchDefinition definition = new Tag(UUID.randomUUID(), "Test", "TEST, !TEST_OTHER").getPatternSearchDefinition();
        assertEquals(2, definition.getArgs().length);
        assertEquals("*TEST*", definition.getArgs()[0]);
        assertEquals("*TEST_OTHER*", definition.getArgs()[1]);
        assertEquals("(LOWER(content) GLOB LOWER(?) AND LOWER(content) NOT GLOB LOWER(?))", definition.getWhereClause());
    }

    @Test
    void shouldGenerateNotClause() {
        PatternSearchDefinition definition = new Tag(UUID.randomUUID(), "Test", "!TEST").getPatternSearchDefinition();
        assertEquals(1, definition.getArgs().length);
        assertEquals("*TEST*", definition.getArgs()[0]);
        assertEquals("(LOWER(content) NOT GLOB LOWER(?))", definition.getWhereClause());
    }
}