package com.dedicatedcode.paperspace;

import com.dedicatedcode.paperspace.model.*;
import com.dedicatedcode.paperspace.tags.Tag;
import com.dedicatedcode.paperspace.tags.TagService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
class ManageControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private BinaryService binaryService;
    @Autowired
    private TagService tagService;
    @Autowired
    private DocumentService documentService;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    void shouldClearAllTagsOnEmptyContent() throws Exception {
        Tag tag = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        this.tagService.store(tag);

        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        TaskDocument taskDocument = new TaskDocument(UUID.randomUUID(), LocalDateTime.now(), "Test Task Title", null, storeBinary(), State.OPEN, Collections.emptyList(), null, null, tags);

        assertFalse(this.documentService.store(taskDocument).getTags().isEmpty());
        this.mockMvc.perform(post("/tag/save")
                        .content("[]")
                        .contentType("application/json"))
                .andDo(print())
                .andExpectAll(status().is(200));

        Document storedDocument = this.documentService.getDocument(taskDocument.getId());
        assertTrue(storedDocument.getTags().isEmpty());
    }

    @Test
    void shouldDeleteExistingTags() throws Exception {
        Tag tag = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        Tag toBeRemoved = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        this.tagService.store(tag);
        this.tagService.store(toBeRemoved);
        List<Tag> allKnownTags = this.tagService.getAll();

        assertTrue(allKnownTags.size() >= 2);

        allKnownTags.remove(toBeRemoved);

        this.mockMvc.perform(post("/tag/save")
                        .content(toString(allKnownTags))
                        .contentType("application/json"))
                .andDo(print())
                .andExpectAll(status().is(200));

        List<Tag> afterUpdate = this.tagService.getAll();
        assertEquals(allKnownTags.size(), afterUpdate.size());

    }

    @Test
    void shouldAddNewTag() throws Exception {
        Tag tag = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        Tag tag2 = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        this.tagService.store(tag);
        this.tagService.store(tag2);
        List<Tag> allKnownTags = this.tagService.getAll();

        String content = toString(allKnownTags);
        int insertIndex = content.indexOf("},{") + 1;
        String newName = "TAG_" + UUID.randomUUID();
        content = content.substring(0, insertIndex) + ",{\"id\":null, \"name\":\""+newName+"\", \"pattern\":\"2000\"}" +  content.substring(insertIndex);
        this.mockMvc.perform(post("/tag/save")
                        .content(content)
                        .contentType("application/json"))
                .andDo(print())
                .andExpectAll(status().is(200));

        List<Tag> afterUpdate = this.tagService.getAll();
        assertEquals(allKnownTags.size() + 1, afterUpdate.size());

        List<Tag> newTags = this.tagService.getAll(newName);
        assertEquals(1, newTags.size());

        Tag newTag = newTags.get(0);
        assertNotNull(newTag.getId());
        assertEquals(newName, newTag.getName());
        assertEquals("2000", newTag.getPattern());
    }

    @Test
    void shouldUpdateExistingTag() throws Exception {
        Tag tag = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        Tag tag2 = new Tag(UUID.randomUUID(), "TAG_" + UUID.randomUUID(), null);
        this.tagService.store(tag);
        this.tagService.store(tag2);
        List<Tag> allKnownTags = this.tagService.getAll();
        allKnownTags.remove(tag);
        allKnownTags.add(tag.withName("UPDATED_" + tag.getName()).withPattern("2000"));

        this.mockMvc.perform(post("/tag/save")
                        .content(toString(allKnownTags))
                        .contentType("application/json"))
                .andDo(print())
                .andExpectAll(status().is(200));

        Tag updatedTag = this.tagService.get(tag.getId());
        assertEquals("UPDATED_" + tag.getName(), updatedTag.getName());
        assertEquals("2000", updatedTag.getPattern());
    }

    private String toString(List<Tag> tags) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(tags);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private Binary storeBinary() {
        Binary binary = new Binary(UUID.randomUUID(),
                LocalDateTime.now(),
                System.getProperty("java.io.tmpdir") + File.separatorChar + UUID.randomUUID() + ".pdf",
                UUID.randomUUID().toString(),
                "application/pdf",
                100, OCRState.OPEN);
        this.binaryService.store(binary);
        return binary;
    }
}