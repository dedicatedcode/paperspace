package com.dedicatedcode.paperspace.tags;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PatternSearchDefinition {

    private final List<SearchPart> parts;

    static PatternSearchDefinition from(Tag tag) {
        return new PatternSearchDefinition(tag);
    }

    List<SearchPart> getParts() {
        return parts;
    }

    private PatternSearchDefinition(Tag tag) {
        this.parts = Arrays.stream(tag.getPattern().split(","))
                .map(String::trim)
                .map(s -> {
                    if (s.startsWith("!")) {
                        return new SearchPart(s.substring(1), true);
                    } else {
                        return new SearchPart(s, false);
                    }
                }).toList();
    }

    public String getWhereClause() {
        return this.parts.stream().map(p -> p.not() ? "LOWER(content) NOT GLOB LOWER(?)" : "LOWER(content) GLOB LOWER(?)").collect(Collectors.joining(" AND ", "(", ")"));
    }

    public Object[] getArgs() {
        return this.parts.stream().map(p -> "*" + p.clause().trim() + "*").toArray();
    }

}
