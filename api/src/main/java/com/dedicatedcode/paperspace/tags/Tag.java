package com.dedicatedcode.paperspace.tags;

import com.dedicatedcode.paperspace.model.Document;
import com.dedicatedcode.paperspace.model.Identifiable;
import org.springframework.util.StringUtils;

import java.beans.Transient;
import java.util.UUID;

public class Tag extends Identifiable {

    private final String name;
    private final String pattern;

    public Tag(UUID id, String name, String pattern) {
        super(id);
        this.name = name;
        this.pattern = pattern;
    }

    public String getName() {
        return name;
    }

    public String getPattern() {
        return this.pattern;
    }

    public Tag withName(String name) {
        return new Tag(getId(), name, pattern);
    }

    public Tag withPattern(String pattern) {
        return new Tag(getId(), name, pattern);
    }

    public boolean matches(Document document) {
        if (StringUtils.hasText(this.pattern) && StringUtils.hasText(document.getContent())) {
            String content = document.getContent();
            return getPatternSearchDefinition().getParts().stream().allMatch(p -> p.matches(content));
        } else {
            return false;
        }
    }

    @Transient
    public PatternSearchDefinition getPatternSearchDefinition() {
        return PatternSearchDefinition.from(this);
    }
}
