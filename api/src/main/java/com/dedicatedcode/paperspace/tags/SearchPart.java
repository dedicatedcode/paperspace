package com.dedicatedcode.paperspace.tags;

record SearchPart(String clause, boolean not) {
    public boolean matches(String description) {
        if (not) {
            return !description.contains(clause);
        } else {
            return description.contains(clause);
        }
    }
}
