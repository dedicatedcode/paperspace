package com.dedicatedcode.paperspace.tags;

import com.dedicatedcode.paperspace.model.Document;
import com.dedicatedcode.paperspace.model.ModifyingDocumentListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentAutoTagger implements ModifyingDocumentListener {
    private final TagService tagService;
    private final boolean enabled;

    public DocumentAutoTagger(TagService tagService, @Value("${tags.automatic.enabled:false}") boolean enabled) {
        this.tagService = tagService;
        this.enabled = enabled;
    }

    @Override
    public Document created(Document document) {
        if (enabled) {
            List<Tag> allTags = tagService.getAll();
            List<Tag> tagsForDocument = allTags.stream().filter(t -> t.matches(document)).toList();
            return document.withTags(tagsForDocument);
        }
        return document;

    }
}
