package com.dedicatedcode.paperspace.search;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SolrConfiguration {
    @Bean
    public SolrClient solrClient(@Value("${spring.data.solr.host}") String baseSolrUrl) {
        return new HttpSolrClient.Builder(baseSolrUrl).build();
    }
}
