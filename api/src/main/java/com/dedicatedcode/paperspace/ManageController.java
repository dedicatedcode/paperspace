package com.dedicatedcode.paperspace;

import com.dedicatedcode.paperspace.model.Document;
import com.dedicatedcode.paperspace.search.SolrService;
import com.dedicatedcode.paperspace.tags.Tag;
import com.dedicatedcode.paperspace.tags.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Controller
public class ManageController {
    private static final Logger log = LoggerFactory.getLogger(ManageController.class);

    private final TagService tagService;
    private final SolrService solrService;
    private final DocumentService documentService;

    public ManageController(TagService tagService, SolrService solrService, DocumentService documentService) {
        this.tagService = tagService;
        this.solrService = solrService;
        this.documentService = documentService;
    }

    @RequestMapping("/manage.html")
    public String manage(Model model) {
        return "manage";
    }

    @RequestMapping("/manage/tags.html")
    public String manageTags(Model model) {
        model.addAttribute("tags", tagService.getAll());
        return "tags";
    }

    @PostMapping(value = "/tag/save", consumes = "application/json")
    @ResponseBody
    public String saveTags(@RequestBody(required = false) List<Tag> tags) {
        if (tags == null || tags.isEmpty()) {
            log.warn("Tags list is empty! Will delete all existing tags");
            this.tagService.getAll().forEach(this.tagService::delete);
            return "OK";
        }

        List<Tag> tagsToDelete = this.tagService.getAll().stream().filter(t -> tags.stream().filter(tag -> tag.getId() != null).noneMatch(tag -> tag.getId().equals(t.getId()))).toList();
        log.info("Found [{}] existing tags to delete", tagsToDelete.size());

        tagsToDelete.forEach(this.tagService::delete);

        for (Tag tag : tags) {
            if (tag.getId() != null) {
                Tag existingTag = this.tagService.get(tag.getId());
                this.tagService.update(existingTag.withName(tag.getName()).withPattern(tag.getPattern()));
            } else {
                this.tagService.store(new Tag(UUID.randomUUID(), tag.getName(), tag.getPattern()));
            }
        }
        return "OK";
    }


    @PostMapping("/re-index")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public String reindex() {
        this.solrService.reindex();
        return "OK";
    }

    @PostMapping("/re-tag")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public String reTagDocuments() {
        List<Tag> allTags = this.tagService.getAll();
        log.info("starting re-tagging with [{}] tags", allTags.size());
        allTags.stream().filter(t -> StringUtils.hasText(t.getPattern())).forEach(t -> {
            List<Document> documents = this.documentService.searchForPattern(t.getPatternSearchDefinition());
            log.info("found [{}] documents for tag-pattern [{}]", documents.size(), t.getPattern());
            documents.stream()
                    .filter(d -> !d.getTags().contains(t))
                    .map(d -> {
                        List<Tag> newTags = d.getTags();
                        newTags.add(t);
                        return d.withTags(newTags);
                    })
                    .forEach(documentService::update);
        });
        this.solrService.reindex();
        return "OK";
    }

    @PostMapping("/clear-tags")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String removeTags() {
        this.documentService.clearTags();
        this.solrService.reindex();
        return "OK";
    }

}
