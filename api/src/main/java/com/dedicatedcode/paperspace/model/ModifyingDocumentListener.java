package com.dedicatedcode.paperspace.model;

public interface ModifyingDocumentListener {
    default Document changed(Document oldVersion, Document newVersion) {
        return newVersion;
    }

    default Document created(Document document) {
        return document;
    }

}
