package com.dedicatedcode.paperspace.model;

public interface DocumentListener {
    default void changed(Document oldVersion, Document newVersion) {

    }

    default void created(Document taskDocument) {

    }

    default void deleted(Document document) {

    }
}
