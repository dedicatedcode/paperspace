package com.dedicatedcode.paperspace.feeder;

public enum EventType {
    DELETE,
    MOVE,
    CREATE,
    EXISTING, CHANGE
}
