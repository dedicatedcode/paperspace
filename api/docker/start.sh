echo 'make sure language is installed and starting application'
apt-get install -yyy tesseract-ocr-${OCR_LANGUAGE}
mkdir -p /storage/database/
echo 'make sure permissions on folders are set correctly'
chown -R paperspace:paperspace /storage
JAVA_OPTS=""

if [ $ENABLE_DEBUGGER == "true" ]; then
  JAVA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8998"
fi
echo "starting application with JAVA_OPTS: ${JAVA_OPTS}"
su paperspace -c "java ${JAVA_OPTS} -jar /app.jar"
